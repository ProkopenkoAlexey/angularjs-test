### Запуск приложения

* install [node.js](https://nodejs.org)
* install [bower](https://bower.io/)
* `npm install`
* `bower install`
* `npm start`

### Задание
* страница 1 - [график](https://github.com/pablojim/highcharts-ng)
![](chart.png) 
* страница 2 - [таблица](http://ui-grid.info/)
![](grid.png)
* При нажатии на кнопку kill увеличивается значение `killed` в этой же строке, при нажатии на save увеличивается значение `saved` соответственно.
* Числа на графике должны всегда совпадать с числами в таблице